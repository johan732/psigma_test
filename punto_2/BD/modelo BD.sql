-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2020 at 11:01 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chais_sale`
--

-- --------------------------------------------------------

--
-- Table structure for table `ejecutivos_comercial`
--

CREATE TABLE `ejecutivos_comercial` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ejecutivos_comercial`
--

INSERT INTO `ejecutivos_comercial` (`id`, `nombre`) VALUES
(1, 'Ejecutivo 1'),
(2, 'Ejecutivo 2'),
(3, 'Ejecutivo 3'),
(4, 'Ejecutivo 4');

-- --------------------------------------------------------

--
-- Table structure for table `sillas_ejecutivo`
--

CREATE TABLE `sillas_ejecutivo` (
  `id` int(11) NOT NULL,
  `ejecutivos_comercial_id` int(11) NOT NULL,
  `fecha_venta` date DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sillas_ejecutivo`
--

INSERT INTO `sillas_ejecutivo` (`id`, `ejecutivos_comercial_id`, `fecha_venta`, `cantidad`, `precio`) VALUES
(1, 1, '2020-07-01', 20, 7000),
(2, 2, '2020-07-02', 11, 5500),
(3, 1, '2020-07-03', 47, 7500),
(4, 1, '2020-07-02', 5, 5900);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ejecutivos_comercial`
--
ALTER TABLE `ejecutivos_comercial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sillas_ejecutivo`
--
ALTER TABLE `sillas_ejecutivo`
  ADD PRIMARY KEY (`id`,`ejecutivos_comercial_id`),
  ADD KEY `fk_sillas_ejecutivo_ejecutivos_comercial_idx` (`ejecutivos_comercial_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ejecutivos_comercial`
--
ALTER TABLE `ejecutivos_comercial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sillas_ejecutivo`
--
ALTER TABLE `sillas_ejecutivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sillas_ejecutivo`
--
ALTER TABLE `sillas_ejecutivo`
  ADD CONSTRAINT `fk_sillas_ejecutivo_ejecutivos_comercial` FOREIGN KEY (`ejecutivos_comercial_id`) REFERENCES `ejecutivos_comercial` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
