<?php

/*-------------------------
Autor: Johan Córdoba
---------------------------*/

class Database
{
    private $con;
    private $dbhost = "localhost";
    private $dbuser = "root";
    private $dbpass = "root";
    private $dbname = "chais_sale";

    function __construct()
    {
        $this->connect_db();
    }

    public function connect_db()
    {
        $this->con = mysqli_connect($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
        if (mysqli_connect_error()) {
            die("La conexión a la base de datos falló " . mysqli_connect_error() . mysqli_connect_errno());
        }
    }

    public function getSillasVendidas($ejecutivo_id = null)
    {
        $sql = "SELECT SUM(cantidad) As total_sillas FROM sillas_ejecutivo";

        if ($ejecutivo_id) {
            $sql .= ' WHERE ejecutivos_comercial_id = ' . $ejecutivo_id;
        }

        $res = mysqli_query($this->con, $sql);
        return $res;
    }

    public function getEjecutivos()
    {
        $sql = "SELECT * FROM ejecutivos_comercial ORDER BY nombre ASC";
        $res = mysqli_query($this->con, $sql);
        return $res;
    }

    public function getVentasSillas()
    {
        $sql = "SELECT *, (cantidad*precio) AS total FROM sillas_ejecutivo AS s INNER JOIN ejecutivos_comercial AS  c ON s.ejecutivos_comercial_id = c.id ORDER BY s.fecha_venta DESC";
        $res = mysqli_query($this->con, $sql);
        return $res;
    }

    public function getTotalVendido($ejecutivo_id = null)
    {
        $sql = "SELECT SUM(cantidad) AS cantidad ,SUM(cantidad*precio) As total_vendido FROM sillas_ejecutivo";

        if ($ejecutivo_id) {
            $sql .= ' WHERE ejecutivos_comercial_id = ' . $ejecutivo_id;
        }

        $res = mysqli_query($this->con, $sql);
        return $res;
    }


}

?>