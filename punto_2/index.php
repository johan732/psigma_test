<?php
include('database.php');
$conexion = new Database();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chairs sale list</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <?php
                $totalSillas = mysqli_fetch_object($conexion->getSillasVendidas());
                ?>
                <div class="col-sm-8">
                    <h2>
                        <small>Total de sillas vendidas:</small>
                        <label class="text-bold"><?= $totalSillas->total_sillas ? $totalSillas->total_sillas : '-' ?></label>
                    </h2>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8">
                        <h3>
                            Listado de ventas por <label class="text-bold">ejecutivo comercial</label>
                        </h3>
                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Ejecutivo</th>
                    <th>Fecha de venta</th>
                    <th>Precio (c/u)</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                </tr>
                </thead>

                <tbody>
                <?php
                $ventas = $conexion->getVentasSillas();
                $totalSillasSum = 0;
                $totalVendidoSum = 0;
                ?>
                <?php while ($row = mysqli_fetch_object($ventas)) { ?>
                    <tr>
                        <td><?= $row->nombre ?></td>
                        <td><?= $row->fecha_venta ?></td>
                        <td>$<?= $row->precio ?></td>
                        <td class="text-center"><?= $row->cantidad ?></td>
                        <td>$<?= $row->total ?></td>
                    </tr>
                    <?php $totalSillasSum += $row->cantidad; ?>
                    <?php $totalVendidoSum += $row->total; ?>
                <?php } ?>
                <tr>
                    <td colspan="3" class="text-right">
                        <h4>Total:</h4>
                    </td>
                    <td class="text-center">
                        <h4> <?= $totalSillasSum ?></h4>
                    </td>
                    <td class="text-center">
                        <h4>$<?= $totalVendidoSum ?></h4>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="form-group">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8">
                        <h3>
                            Ventas por <label class="text-bold">ejecutivo comercial</label>
                        </h3>
                    </div>
                </div>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Ejecutivo</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                </tr>
                </thead>

                <tbody>
                <?php
                $ejecutivos = $conexion->getEjecutivos();
                ?>
                <?php while ($row = mysqli_fetch_object($ejecutivos)) { ?>
                    <?php $totalVendido = mysqli_fetch_object($conexion->getTotalVendido($row->id)); ?>
                    <tr>
                        <td><?= $row->nombre ?></td>
                        <td class="text-center"><?= $totalVendido->cantidad ? $totalVendido->cantidad : '-' ?></td>
                        <td class="text-center"><?= $totalVendido->total_vendido ? '$'.$totalVendido->total_vendido : '-' ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
</body>
</html>