<?php
function siglo_del_anio($anio)
{
    if ($anio < 1 or $anio >= 2005) {
        return null;
    }

    $year = str_pad($anio, 4, "0", STR_PAD_LEFT);
    $siglo = substr($year, 0, 2) + 0;
    $limit = substr($year, 2, 4);

    if ($limit > 0) {
        $siglo += 1;
    }

    return $siglo;
}

echo siglo_del_anio(1905) . "<br>";
echo siglo_del_anio(1700) . "<br>";
echo siglo_del_anio(1701) . "<br>";
echo siglo_del_anio(101) . "<br>";
echo siglo_del_anio(200) . "<br>";
echo siglo_del_anio(-1) . "<br>";
echo siglo_del_anio(2006) . "<br>";
echo siglo_del_anio(100) . "<br>";
echo siglo_del_anio(101) . "<br>";
echo siglo_del_anio(1700) . "<br>";
?>